#!/usr/local/bin/bash

ping_args="-q -w 1 -c 5 -i 0.25"
data=""

ping () {
 res=`/sbin/ping $ping_args $1`
 fill_json "$1" "$res" "ipv4"
}
ping6 () {
 res=`/sbin/ping6 $ping_args $1`
 fill_json "$1" "$res" "ipv6"
}

fill_json () {
 loss=`echo $2 | awk -F'[=/ ]' '{print $18}' | sed 's/%//'`
 val=`echo $2 | awk -F'[=/ ]' '{print $28}'`
 min=${val:=0}
 val=`echo $2 | awk -F'[=/ ]' '{print $29}'`
 avg=${val:=0}
 val=`echo $2 | awk -F'[=/ ]' '{print $30}'`
 max=${val:=0}
 ip=`echo $2 | awk -F'[=/ ]' '{print $3}' | sed 's/(//;s/)://'`
 if [ "$data" != "" ]; then data=$data", "; fi
 data=$data'{ "measurement": "ping", "tags": { "dest": "'$1'", "ip": "'$ip'", "type": "loss", "af": "'$3'" }, "fields": { "value":'$loss' }},{ "measurement": "ping", "tags": { "dest": "'$1'", "ip": "'$ip'", "type": "min", "af": "'$3'" }, "fields": { "value":'$min' }},{ "measurement": "ping", "tags": { "dest": "'$1'", "ip": "'$ip'", "type": "avg", "af": "'$3'" }, "fields": { "value":'$avg' }},{ "measurement": "ping", "tags": { "dest": "'$1'", "ip": "'$ip'", "type": "max", "af": "'$3'" }, "fields": { "value":'$max' }}'
}

ping openstreetmap.rezopole.net
ping6 openstreetmap.rezopole.net

data="["$data"]"
curl -XPOST -k --data-binary "$data" https://influx_server